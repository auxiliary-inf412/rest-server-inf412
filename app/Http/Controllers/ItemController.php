<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Item::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Item();
        $item->description = $request->description;
        $item->price = $request->price;
        $item->status = true;
        $item->save();
        return [
            'msg' => 'item save successfully'
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::find($id);
        $item->description = $request->description;
        $item->price = $request->price;
        $item->status = $request->status;
        $item->update();
        return [
            'msg' => 'item updated successfully'
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::where('id', $id);
        $item->delete();
        return [
            'msg' => 'item removed successfully'
        ];
    }

    public function exportReport(Request $request)
    {
        return Excel::download(new Item(), 'export-items.xlsx');
    }
}
