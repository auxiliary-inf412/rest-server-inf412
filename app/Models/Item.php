<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\FromCollection;

class Item extends Model implements FromCollection
{
    use HasFactory;

    protected $table = 'items';

    protected $fillable = [
        'description',
        'price',
        'status',
    ];

    public function collection()
    {
        return Item::all();
    }
}
